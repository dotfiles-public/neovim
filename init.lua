require("keymaps") -- [gf:~/.config/nvim/lua/keymaps.lua]

local PLUGINS_ENABLED = true
vim.g.mapleader = " "

-- Custom neovim options
vim.opt.backup = false
vim.opt.clipboard = "unnamedplus"
vim.opt.ignorecase = true
vim.opt.encoding = "utf-8"
vim.opt.termguicolors = true
vim.opt.timeoutlen = 500
vim.opt.hidden = true
vim.opt.confirm = true
vim.opt.cursorline = true
vim.opt.inccommand = "split"
vim.opt.mouse = "a"
vim.opt.showmode = false
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.numberwidth = 1
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.expandtab = true
vim.opt.smartindent = false
vim.opt.breakindent = true
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.expandtab = true
vim.opt.formatoptions = "1"
vim.opt.linebreak = true
vim.opt.wrap = false
vim.opt.laststatus = 3
vim.opt.colorcolumn = "80"
vim.opt.fcs = "eob: "

----- Plugin manager configuration
-- If the plugin option is false, break the execution
if PLUGINS_ENABLED then
   require("plug") -- [gf:~/.config/nvim/lua/plug.lua]
end

-- [gf:~/.config/nvim/lua/utils.lua]
local utils = require("utils")
local augroup = utils.augroup

-- Autocommand to:
-- * Setup the color of some elements
vim.api.nvim_create_autocmd("BufEnter", {
   group = augroup,

   callback = function()
      vim.cmd [[
         hi WinSeparator guibg=none guifg=#494D64 " color: window separators color
         hi ColorColumn  guibg=#282E38            " color: line break column background
         hi CursorLine   guibg=#282E38            " color: cursor background
      ]]
   end,
})

-- Command to create a tags files to not depend o LSP everytime
vim.api.nvim_create_user_command("MakeTags", function()
   io.popen("ctags -R --exclude=.git --exclude=.node_modules")
end, {})

--- Easely way to setup a new vim/neovim mapping for the user
--- @param size  integer      Indentation size
--- @param files string|table List of files to indent
local function indentbuf(size, files)
   vim.api.nvim_create_autocmd("BufWinEnter", {
      group = augroup,
      pattern = files,

      callback = function()
         vim.opt.shiftwidth = size
         vim.opt.tabstop = size
      end,
   })
end

indentbuf(2, { "*.html", "*.json", "*.jsx", "*.tsx", "*.astro", "*.vue" })
indentbuf(3, "*.lua")
indentbuf(8, { "todo.txt", "*.cc", "*.java" })

--- Update the syntaxe of some specific file
--- @param files  string|table Buffer name
--- @param syntax string       Syntaxe name
local function setsyntax(files, syntax)
   vim.api.nvim_create_autocmd("BufWinEnter", {
      group = augroup,
      pattern = files,

      callback = function()
         vim.opt.ft = syntax
      end,
   })
end

setsyntax("*.txt", "help")
setsyntax("todo.txt", "todotxt")
setsyntax(".zshrc", "bash")
