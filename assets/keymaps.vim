let mapleader="\<space>"
inoremap <c-l> <esc>
vnoremap <c-l> <esc>
nnoremap j gj
nnoremap k gk
nnoremap Y yg$
nnoremap <c-s> :w!<cr>
inoremap <c-s> <esc>:w!<cr>
vnoremap <c-s> :w!<cr>
nnoremap <leader>q :q<cr>
nnoremap <leader>Q :qa!<cr>
nnoremap <leader>T :tabnew<cr>
nnoremap <leader>h <c-w>h
nnoremap <leader>j <c-w>j
nnoremap <leader>k <c-w>k
nnoremap <leader>l <c-w>l
nnoremap <leader>I :vsplit<cr>
nnoremap <leader>S :split<cr>
nnoremap <left> :vertical resize +1<cr>
nnoremap <down> :resize -1<cr>
nnoremap <up> :resize +1<cr>
nnoremap <right> :vertical resize -1<cr>
nnoremap <tab> :bn<cr>
nnoremap <s-tab> :bp<cr>
nnoremap <leader><tab> :tabnext<cr>
nnoremap <leader><s-tab> :tabprevious<cr>
nnoremap <leader>bd :bdelete<cr>
inoremap <c-k> <cr><esc>O
nnoremap <leader><c-w> :setl wrap!<cr>
nnoremap <leader>, A,<esc>
nnoremap <leader>; A;<esc>
nnoremap <leader>: :set number!<cr>
nnoremap <leader>gg mzgg=G"z
nnoremap <leader><leader> @
nnoremap <leader>rpl :%s/
