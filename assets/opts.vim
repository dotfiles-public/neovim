" Basic options
set ignorecase nobackup
set hidden confirm noshowmode
set splitbelow splitright number relativenumber cursorline

set clipboard^=unnamed,unnamedplus
set termguicolors encoding=UTF-8
set timeoutlen=500 mouse=a

set expandtab nosmartindent breakindent linebreak
set shiftwidth=4 tabstop=4 formatoptions=1
set term=xterm-256color

colorscheme pablo
set fcs=eob:\ 

" Plugin setup (with out any plugin manager)
filetype plugin on
syntax enable
source ~/.config/nvim/assets/auto-pairs.vim

set nocompatible
set path+=**
set path-=**/node_modules/**
set wildmenu

" Setting a custom syntaxe for some buf types
au BufWinEnter *.txt set ft=help
au BufWinEnter *.ino set ft=cpp

au BufWinEnter *.html   set shiftwidth=2 tabstop=2 " -> Custom indent size: 2 spaces
au BufWinEnter *.json   set shiftwidth=2 tabstop=2
au BufWinEnter *.jsx    set shiftwidth=2 tabstop=2
au BufWinEnter *.tsx    set shiftwidth=2 tabstop=2
au BufWinEnter *.astro  set shiftwidth=2 tabstop=2
au BufWinEnter *.vue    set shiftwidth=2 tabstop=2
au BufWinEnter *.lua    set shiftwidth=3 tabstop=3 " -> Custom indent size: 3 spaces
au BufWinEnter todo.txt set shiftwidth=8 tabstop=8 " -> Custom indent size: 8 spaces
au BufWinEnter *.cc     set shiftwidth=8 tabstop=8
au BufWinEnter *.java   set shiftwidth=8 tabstop=8

" Command to make a tags file to emulate LSP jump-to-definition
command! MakeTags !ctags -R --exclude=.git --exclude=node_modules

" Tweaks for browsing in NETRW
let g:netrw_banner=0       " disable annoying banner
let g:netrw_liststyle=3    " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide=',\(^\|\s\s\)\zs\.\S\+'

" IMPORTING THE MAPPINGS FILE
source ~/.config/nvim/assets/keymaps.vim

" Show the autocomplete window when clicked
inoremap / /<c-x><c-f><c-p>
inoremap . .<c-n><c-p>
inoremap ( (<c-n><c-p>
inoremap [ [<c-n><c-p>
inoremap { {<c-n><c-p>
inoremap , ,<c-n><c-p>
inoremap ) )<c-n><c-p>
inoremap ] ]<c-n><c-p>
inoremap } }<c-n><c-p>

" Fuzzy search like binding
nnoremap <leader>sf :find ./**/

" Open the directory tree
nnoremap <leader>n :edit .<cr>

" Mapping to open the terminal emulator
nnoremap <leader>t :term<cr>

" Show a help of possible commands
nnoremap <leader>? :!sed -e "/^map/\!d; s/[\",'])$//" -e "s/^map([\",']/[/; s/[\",'],\ *[\",']/]\t/" -e "s/[\",'],\ *[\",']/ -> /" -e "s/[\",'],\ *[\",']/\t\t\|> /" < $HOME/.config/nvim/lua/keymaps.lua \| less<cr>
