function __sed_keymaps {
    sed -e "/^map/!d" \
        -e "s/map([\",']//; s/[\",'],\ *[\",']/noremap /" \
        -e "s/[\",'],\ *[\",']/ /; s/[\",'],\ *[\",'].*$//g" \
        < lua/keymaps.lua
}

function buildmaps {
    echo 'let mapleader="\<space>"' > assets/keymaps.vim
    __sed_keymaps >> assets/keymaps.vim
}

function vimrc {
    if [ -e $HOME/.vimrc ]; then
        echo "The .vimrc file already exists, creating a .vimrc.backup file..."
        mv $HOME/.vimrc $HOME/.vimrc.backup || exit 1
    fi
    ln -s $HOME/.config/nvim/assets/opts.vim $HOME/.vimrc
}

if [ -z $1 ]; then
    echo
    echo " AVAILABLE COMMANDS"
    echo "  buildmaps: create a assets/keymaps.vim with lua/keymaps.lua bidings"
    echo "  vimrc:     link the assets/opts.vim to ~/.vimrc file"
else
    $1
fi
