local map = require("utils").map
vim.g.mapleader = " "

-- general propurse

map("i", "<c-l>", "<esc>", "CUSTOM: Better <esc> for insert mode")
map("v", "<c-l>", "<esc>", "CUSTOM: Better <esc> for visual mode")

map("n", "j", "gj",  "CUSTOM: Move the cursor down in the visual lines")
map("n", "k", "gk",  "CUSTOM: Move the cursor up in the visual lines")
map("n", "Y", "yg$", "CUSTOM: Yank the whole line after the cursor")

map("n", "<c-s>", ":w!<cr>",      "CUSTOM: Save the file")
map("i", "<c-s>", "<esc>:w!<cr>", "CUSTOM: Save the file (insert mode)")
map("v", "<c-s>", ":w!<cr>",      "CUSTOM: Save the file (normal mode)")

map("n", "<leader>q", ":q<cr>",   "CUSTOM: Exit")
map("n", "<leader>Q", ":qa!<cr>", "CUSTOM: Force the exiting")

-- window, buffer and tab management

map("n", "<leader>T", ":tabnew<cr>", "CUSTOM: Create a new tab")

map("n", "<leader>h", "<c-w>h", "CUSTOM: Select the left window")
map("n", "<leader>j", "<c-w>j", "CUSTOM: Select the bottom window")
map("n", "<leader>k", "<c-w>k", "CUSTOM: Select the top window")
map("n", "<leader>l", "<c-w>l", "CUSTOM: Select the right window")

map("n", "<leader>I", ":vsplit<cr>", "CUSTOM: Split the window verticaly (new collumn)")
map("n", "<leader>S", ":split<cr>",  "Split the window horizontaly (new row)")

map("n", "<left>",  ":vertical resize +1<cr>", "CUSTOM: Increase the window size verticaly")
map("n", "<down>",  ":resize -1<cr>",          "CUSTOM: Decrease the window size horizontaly")
map("n", "<up>",    ":resize +1<cr>",          "CUSTOM: Increase the window size horizontaly")
map("n", "<right>", ":vertical resize -1<cr>", "CUSTOM: Decrease the window size verticaly")

map("n", "<tab>",           ":bn<cr>",          "CUSTOM: Go to next buffer")
map("n", "<s-tab>",         ":bp<cr>",          "CUSTOM: Go to previous buffer")
map("n", "<leader><tab>",   ":tabnext<cr>",     "CUSTOM: Select the next tab")
map("n", "<leader><s-tab>", ":tabprevious<cr>", "CUSTOM: Select the previous tab")

map("n", "<leader>bd", ":bdelete<cr>", "CUSTOM: Delete the current buffer")

-- hacking and personal

map("i", "<c-k>",         "<cr><esc>O",      "CUSTOM: Do an return but back to upper line and indent")
map("n", "<leader><c-w>", ":setl wrap!<cr>", "CUSTOM: Toggle the word wrap option")

map("n", "<leader>,", "A,<esc>",          "Insert a comma in the end of the line")
map("n", "<leader>;", "A;<esc>",          "CUSTOM: Insert a collon in the end of the line")
map("n", "<leader>:", ":set number!<cr>", "CUSTOM: Toggle the line numbers")

map("n", "<leader>gg",       'mzgg=G"z', "CUSTOM: Auto indent the whole file text")
map("n", "<leader><leader>", "@",        "CUSTOM: Emulate the macro command")
map("n", "<leader>rpl",      ":%s/",     "CUSTOM: Shortcut to ubistitute a string to another")
