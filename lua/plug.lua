local CONFIG_PATH = os.getenv("HOME").."/.config/nvim"

local map = require("utils").map

map("n", "<leader>t",  ":ToggleTerm<cr>", "PLUGIN: Open the a terminal emulator")

map("n", "<leader>sf",  require("telescope.builtin").find_files,  "PLUGIN: Find files")
map("n", "<leader>sgc", require("telescope.builtin").git_commits, "PLUGIN: List the project git commits")
map("n", "<leader>sgs", require("telescope.builtin").git_status,  "PLUGIN: List the git status changes on this file")
map("n", "<leader>ss",  require("telescope.builtin").grep_string, "PLUGIN: Search in this current buffer")
map("n", "<leader>sb",  require("telescope.builtin").buffers,     "PLUGIN: Find a buffer")
map("n", "<leader>sm",  require("telescope.builtin").marks,       "PLUGIN: Search for a vim mark")
map("n", "<leader>?",   ":Telescope keymaps<cr>",                 "PLUGIN: List all bidings configured")

map("n", "<leader>r", vim.lsp.buf.rename,     "LSP: Rename a variable")
map("n", "<leader>]", vim.lsp.buf.definition, "LSP: Jump to definition under cursor")
map("n", "<leader>[", vim.lsp.buf.references, "LSP: List all references of that definition")
map("n", "<leader>L", vim.lsp.buf.format,     "LSP: Auto format the text file")
map("n", "<leader>D", vim.lsp.buf.hover,      "LSP: Show docummentation for the symbol under the cursor")

map("n", "<cr>", ":Commentary<cr>",     "PLUGIN: Comment the current line")
map("v", "<cr>", ":'<'>Commentary<cr>", "PLUGIN: Comment the current selected lines")

--- Function that returns a table with a list of all files in a directory
--- @param dir_path string Directory to list and get the files
local function list_dir(dir_path)
   local i, result = 1, {}
   local dir = io.popen("ls -A "..dir_path)

   if dir ~= nil then
      for file in dir:lines() do
         result[i] = file
         i = i + 1
      end
      dir:close()
   end

   return result
end


-- Packer pluggin manager startup configuration
require("packer").startup {
   function(use)
      use("wbthomason/packer.nvim")
      use("kyazdani42/nvim-web-devicons")

      ----- Table with all the plugin files
      -- [gf:~/.config/nvim/lua/plugins]
      local plugins = list_dir(CONFIG_PATH.."/lua/plugins")

      -- Repeat that for all files in the plugins directory
      for _, file in ipairs(plugins) do
         local basename = file:gsub(".lua", "")       -- Remove the ".lua" extension
         local plugin = require("plugins."..basename) -- Import by that file name
         use(plugin)                                  -- Install/config the given plugin
      end

      -- Plugins that need no configuration
      use({ "windwp/nvim-autopairs",   config = require("nvim-autopairs").setup() })
      use({ "akinsho/toggleterm.nvim", config = require("toggleterm").setup() })
      use("tpope/vim-commentary")
      use("tpope/vim-surround")
      use("mattn/emmet-vim")
   end,
}

-- emmet plugin config
vim.g.user_emmet_install_global = false
vim.g.user_emmet_leader_key = ","

-- My personal group to run autocommands
local user_group = vim.api.nvim_create_augroup("UserGroup", { clear = true })

-- install emmet for specific buffers
vim.api.nvim_create_autocmd("BufWinEnter", {
   group = user_group,
   command = "EmmetInstall",
   pattern = {
      "*.html",  "*.css",  "*.js",
      "*.scss",  "*.sass", "*.md",
      "*.ts",    "*.jsx",  "*.tsx",
      "*.astro", "*.vue",
   },
})
