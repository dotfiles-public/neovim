local utils = {
   augroup = vim.api.nvim_create_augroup("UserGroup", { clear = true })
}

--- Easely way to setup a new vim/neovim mapping for the user
--- @param mode string          Vim mode to set the keybind
--- @param keys string          Key sequence to execute
--- @param func string|function Function or command to run
--- @param desc string          Useful description of that biding
function utils.map(mode, keys, func, desc)
   vim.keymap.set(mode, keys, func, {
      buffer = nil,
      desc = desc,
      noremap = true,
   })
end

return utils
