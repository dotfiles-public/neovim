local plugin = {
   "neovim/nvim-lspconfig",

   requires = {
      -- Automatically install LSPs to stdpath for neovim
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim",

      -- Useful status updates for LSP
      "j-hui/fidget.nvim",

      -- Additional lua configuration, makes nvim stuff amazing
      "folke/neodev.nvim",
   },
}

function plugin.config()
   require("mason").setup()
   require("neodev").setup()
   require("fidget").setup()

   local servers = {
   }

   local capabilities = vim.lsp.protocol.make_client_capabilities()
   capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

   local mason_lspconfig = require("mason-lspconfig")

   mason_lspconfig.setup {
      ensure_installed = vim.tbl_keys(servers)
   }

   mason_lspconfig.setup_handlers {
      function(server_name)
         require("lspconfig")[server_name].setup {
            capabilities = capabilities,
            -- on_attach = function(_, bufnr) return end,
            settings = servers[server_name],
         }
      end,
   }
end

return plugin
