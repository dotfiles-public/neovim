local plugin = { "lukas-reineke/indent-blankline.nvim" }

function plugin.config()
   require("indent_blankline").setup({})
end

return plugin
