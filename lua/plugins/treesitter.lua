local plugin = { "nvim-treesitter/nvim-treesitter" }

function plugin.config()
   require("nvim-treesitter.configs").setup {
      sync_install = true,

      highlight = {
         enable = true,
         disable = { 'html', 'vim' },
         additional_vim_regex_highlighting = true,
      },

      indent = {
         enable = true,
         disable = { 'scss', 'css', 'sass' },
      },
   }
end

return plugin
