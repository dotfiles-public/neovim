local plugin = { "shaunsingh/nord.nvim" }

function plugin.config()
   vim.g.nord_italic = false
   require("nord").set()
end

return plugin
