local plugin = {
   "akinsho/bufferline.nvim"
}

function plugin.config()
   local pallet = {
      elem_fg = "#6c7086",
      selected_bg = "",
      error_fg = "#f38ba8",
      warning_fg = "#f9e2af",
      info_fg = "#89b4fa",
      pick_f = "#181825",
      bar_bg = "#282E38",
      bar_fg = "",
      elem_bg = "#181825",
      selected_fg = "",
   }

   local colors = {
      warning = { fg = pallet.warning_fg, bg = pallet.elem_bg, sp = pallet.warning_fg },
      error = { fg = pallet.error_fg, bg = pallet.elem_bg, sp = pallet.error_fg },
      info = { fg = pallet.info_fg, bg = pallet.elem_bg, sp = pallet.info_fg },

      warning_selected = { fg = pallet.warning_fg, bg = pallet.selected_bg, gui = '' },
      error_selected = { fg = pallet.error_fg, bg = pallet.selected_bg, gui = '' },
      info_selected = { fg = pallet.info_fg, bg = pallet.selected_bg, gui = '' },

      elem_selected = { fg = pallet.selected_fg, bg = pallet.selected_bg },
      separator_selected = { fg = pallet.selected_bg, bg = pallet.selected_bg },
      pick_selected = { fg = pallet.pick_fg, bg = pallet.selected_bg },
      separator = { fg = pallet.selected_bg, bg = pallet.elem_bg },
      elem = { fg = pallet.elem_fg, bg = pallet.elem_bg },
      elem_inactive = { fg = pallet.elem_fg, bg = pallet.elem_bg },
      pick = { fg = pallet.pick_fg, bg = pallet.elem_bg },
      bar = { fg = pallet.bar_fg, bg = pallet.bar_bg }
   }

   require("bufferline").setup {
      options = {
         max_prefix_length = 20, -- prefix used when a buffer is de-duplicated
         max_name_length = 20,
         diagnostics = 'coc',

         -- can also be a table containing 2 custom separators
         -- [focused and unfocused]. eg: { '|', '|' }

         separator_style = '',
         always_show_bufferline = true,
         enforce_regular_tabs = true,
         show_buffer_close_icons = false,
         show_tab_indicators = true,
         show_buffer_icons = true, -- disable filetype icons for buffers
         show_close_icon = false,

         diagnostics_indicator = function(count)
            return '['..count..']'
         end,

         offsets = {{
            filetype = 'NvimTree',
            text = 'File Explorer',
         }},

         indicator = {
            style = 'icon',
            icon = ' ',
         },
      },

      highlights = {
         separator_selected = colors.separator_selected,
         pick_selected = colors.pick_selected,
         close_button_selected = colors.elem_selected,
         duplicate_selected = colors.elem_selected,
         modified_selected = colors.elem_selected,
         buffer_selected = colors.elem_selected,
         buffer_visible = colors.elem_inactive,
         tab_selected = colors.elem_selected,
         background = colors.elem_inactive,
         separator_visible = colors.separator,
         separator = colors.separator,
         warning_visible = colors.warning,
         warning = colors.warning,
         error = colors.error,
         pick = colors.pick,
         close_button_visible = colors.elem,
         duplicate_visible = colors.elem,
         modified_visible = colors.elem,
         close_button = colors.elem,
         duplicate = colors.elem,
         modified = colors.elem,
         tab = colors.elem,
         hint_visible = colors.info,
         info_visible = colors.info,
         hint = colors.info,
         info = colors.info,
         tab_close = colors.bar,
         fill = colors.bar,

         warning_diagnostic_visible = colors.warning,
         warning_diagnostic = colors.warning,
         error_diagnostic = colors.error,
         hint_diagnostic_visible = colors.info,
         hint_diagnostic = colors.info,
         info_diagnostic_visible = colors.info,
         info_diagnostic = colors.info,
         diagnostic_visible = colors.info,
         diagnostic = colors.info,
      }
   }
end

return plugin
